//
//  AddArticleViewController.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 18/12/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddArticleViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblArticleTitle: UILabel!
    @IBOutlet weak var txtArticleTitle: UITextField!
    @IBOutlet weak var lblArticleDescription: UILabel!
    @IBOutlet weak var textViewArticleDescription: UITextView!
    @IBOutlet weak var btnSaveArticle: UIButton!
    
    var articleId: String?
    var articleTitle: String?
    var imageUrl: String?
    var articleCreatedDate: String?
    var articleDescription: String?
    
    var imageForUpload: UIImage?
    var imagePickerController: UIImagePickerController!
    
    let articleViewModel = ArticleViewModel.shared
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Kh Jrung Thom", size: 16)!]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Kh Jrung Thom", size: 36)!]
        
        navigationItem.title = "add_article_title".localizedString()
        lblArticleTitle.text = "article_title".localizedString()
        lblArticleDescription.text = "article_desc".localizedString()
        btnSaveArticle.setTitle("save_article".localizedString(), for: .normal)
        
        if articleId != nil {
            navigationItem.title = "edit_article_title".localizedString()
            txtArticleTitle.text = articleTitle
            textViewArticleDescription.text = articleDescription
            imageView.kf.setImage(with: URL(string: "\(imageUrl ?? "no image")"), placeholder: UIImage(named: "default_image"), options: nil, progressBlock: nil) { result in
                switch result {
                case .success(_):
                    break
                case .failure(let error):
                    if error.isInvalidResponseStatusCode {
                        self.imageView.image = UIImage(named: "default_image")
                    }
                }
            }
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(pickImage))
        imageView.addGestureRecognizer(tapGesture)
        imageView.isUserInteractionEnabled = true
    }
    
    @objc func pickImage() {
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnSaveArticlePressed(_ sender: Any) {
        
        let articleTitle = txtArticleTitle.text!
        let articleDescription = textViewArticleDescription.text!
        
        if articleTitle != "" && articleDescription != "" {
            if imageForUpload != nil {
                if articleId == nil { // Process to PostArticle()
                    articleViewModel.uploadImage(image: imageForUpload!, completionHandler: { (imageResponse) in
                        self.articleViewModel.postArticle(article: ArticleRequest(title: articleTitle, description: articleDescription, published: true, imageUrl: imageResponse.imageUrl), completionHandler: { (postArticleResponse) in
                            let alert = UIAlertController(title: "Article", message: postArticleResponse.message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.navigationController?.popToRootViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)

                            // Clear data
                            self.txtArticleTitle.text = ""
                            self.textViewArticleDescription.text = ""
                            self.imageView.image = UIImage(named: "default_image")
                            
                            self.navigationController?.popViewController(animated: true)
                        })
                    })
                } else { // Process to UpdateArticle()
                    articleViewModel.uploadImage(image: imageForUpload!, completionHandler: { [self] (imageResponse) in
                        self.articleViewModel.updateArticle(articleId: articleId!, article: ArticleRequest(title: articleTitle, description: articleDescription, published: true, imageUrl: imageResponse.imageUrl), completionHandler: { (updateArticleResponse) in
                            let alert = UIAlertController(title: "Article", message: updateArticleResponse.message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.navigationController?.popToRootViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)

                            // Clear data
                            self.txtArticleTitle.text = ""
                            self.textViewArticleDescription.text = ""
                            self.imageView.image = UIImage(named: "default_image")
                        })
                    })
                }
                
            } else {
                
                if articleId == nil { // Process to PostArticle()
                    articleViewModel.postArticle(article: ArticleRequest(title: articleTitle, description: articleDescription, published: true, imageUrl: "No Image"), completionHandler: { (postArticleResponse) in
                        let alert = UIAlertController(title: "Article", message: postArticleResponse.message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)

                        // Clear data after post article
                        self.txtArticleTitle.text = ""
                        self.textViewArticleDescription.text = ""
                    })
                } else { // Process to UpdateArticle()
                    articleViewModel.updateArticle(articleId: articleId!, article: ArticleRequest(title: articleTitle, description: articleDescription, published: true, imageUrl: imageUrl!), completionHandler: { (updateArticleResponse) in
                        let alert = UIAlertController(title: "Article", message: updateArticleResponse.message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)

                        // Clear data
                        self.txtArticleTitle.text = ""
                        self.textViewArticleDescription.text = ""
                        self.imageView.image = UIImage(named: "default_image")
                        
                    })
                }
            
            }
        } else {
            let alert = UIAlertController(title: "Warning", message: "Please input require field!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension AddArticleViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            imageView.image = image
            imageForUpload = image
        }
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}

extension AddArticleViewController: ArticleDelegate {
    func sendArticleData(id: String, image: String, title: String, createdDate: String, description: String, authorName: String, authorImage: String) {
        articleId = id
        articleTitle = title
        imageUrl = image
        articleCreatedDate = createdDate
        articleDescription = description
    }
}
