//
//  ArticleTableViewCell.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 18/12/20.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var lblArticleTitle: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var lblAuthorUsername: UILabel!
    @IBOutlet weak var lblArticleCreatedDate: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
