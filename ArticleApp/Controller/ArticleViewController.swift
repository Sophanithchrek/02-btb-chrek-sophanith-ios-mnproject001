//
//  ArticleViewController.swift
//  ArticleApp
//
//  Created by Sophanith Chrek on 18/12/20.
//

import UIKit
import Kingfisher

class ArticleViewController: UIViewController {

    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var lblArticleTitle: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblArticleCreatedDate: UILabel!
    @IBOutlet weak var lblArticleDescription: UILabel!
    
    var articleDelegate: ArticleDelegate?
    let articleViewModel = ArticleViewModel.shared
    
    var articleId: String?
    var articleTitle: String?
    var imageUrl: String?
    var articleCreatedDate: String?
    var articleDescription: String?
    var nameOfAuthor: String?
    var imageOfAuthor: String?
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblArticleTitle.text = articleTitle!
        articleImageView.kf.setImage(with: URL(string: "\(imageUrl!)"), placeholder: UIImage(named: "default_image"), options: nil, progressBlock: nil) { result in
            switch result {
            case .success( _):
                break
            case .failure(let error):
                if error.isInvalidResponseStatusCode {
                    self.articleImageView.image = UIImage(named: "image-not-available")
                }
            }
        }
        lblAuthorName.text = nameOfAuthor
        authorImageView.image = UIImage(named: imageOfAuthor!)
        lblArticleCreatedDate.text = articleCreatedDate
        lblArticleDescription.text = articleDescription
    }
    
    @IBAction func btnEditArticlePressed(_ sender: Any) {
        let addArticleVC = storyboard?.instantiateViewController(withIdentifier: "addArticleVC") as! AddArticleViewController
        articleDelegate = addArticleVC
        articleDelegate?.sendArticleData(id: articleId!, image: imageUrl!, title: articleTitle!, createdDate: articleCreatedDate!, description: articleDescription!, authorName: "Default Name", authorImage: "Default Image")
        addArticleVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(addArticleVC, animated: true)
    }
    
    @IBAction func btnDeleteArticleAction(_ sender: Any) {
        let alert = UIAlertController(title: "question_remove".localizedString(), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "yes".localizedString(), style: .destructive, handler: {_ in
            self.articleViewModel.deleteArticle(articleId: self.articleId!, completionHandler: { (deleteArticleResponse) in
                let deleteAlert = UIAlertController(title: deleteArticleResponse.message, message: nil, preferredStyle: .alert)
                deleteAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                self.present(deleteAlert, animated: true, completion: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "no".localizedString(), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
}

extension ArticleViewController: ArticleDelegate {
    func sendArticleData(id: String, image: String, title: String, createdDate: String, description: String, authorName: String, authorImage: String) {
        articleId = id
        articleTitle = title
        imageUrl = image
        articleCreatedDate = createdDate
        articleDescription = description
        nameOfAuthor = authorName
        imageOfAuthor = authorImage
    }
}
