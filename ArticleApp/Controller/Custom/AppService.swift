//
//  AppService.swift
//  CoreDataHomeWork
//
//  Created by SOPHANITH CHREK on 7/12/20.
//

import Foundation
import UIKit

struct AppService {
    static let shared = AppService()
    // Computed properties jea get{}
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    func choose(language: String){
        UserDefaults.standard.set(language, forKey: "lang")
    }
}

enum language: String {
    case english = "en"
    case khmer = "km"
}
