//
//  ProfileViewController.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 18/12/20.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblTotalPost: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var switchLanguageController: UISwitch!
    @IBOutlet weak var lblChooseTheme: UILabel!
    @IBOutlet weak var btnExit: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Kh Jrung Thom", size: 16)!]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Kh Jrung Thom", size: 36)!]
        
        switchElementLanguage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if AppService.shared.language == "en" {
            switchLanguageController.isOn = true
        } else {
            switchLanguageController.isOn = false
        }
    }
    
    func setLang(lang: String){
        AppService.shared.choose(language: lang)
        
        switchElementLanguage()
    }
    
    @IBAction func switchLanguageAction(_ sender: UISwitch) {
        if sender.isOn {
            setLang(lang: language.english.rawValue)
        } else {
            setLang(lang: language.khmer.rawValue)
        }
    }
    
    func switchElementLanguage() {
        navigationItem.title = "profile_title".localizedString()
        lblUsername.text = "user_name".localizedString()
        lblTotalPost.text = "total".localizedString()
        lblLanguage.text = "language".localizedString()
        lblChooseTheme.text = "choose".localizedString()
        btnExit.setTitle("exit".localizedString(), for: .normal)
    }

}


