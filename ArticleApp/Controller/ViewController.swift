//
//  ViewController.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 18/12/20.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtInputTextToSearch: UITextField!
    @IBOutlet weak var segmentContentController: UISegmentedControl!
    
    var articleDelegate: ArticleDelegate?
    let articleViewModel = ArticleViewModel.shared
    var articleData = [ArticleModel]()
    
    var isFetchMore: Bool = false
    var page: Int = 1
    var modelPage = [Int]()
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Kh Jrung Thom", size: 16)!]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Kh Jrung Thom", size: 36)!]
        
        navigationItem.title = "popular_article_title".localizedString()
        txtInputTextToSearch.placeholder = "search".localizedString()
        
        //Refresh Data
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)

    }
    
    @objc func didPullRefresh() {
        isFetchMore = false
        getAllArticle()
        DispatchQueue.main.async {
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorColor = .white

        getArticleByPagination()
    }

    @IBAction func btnSearchArticle(_ sender: Any) {
        
    }
    
    func getAllArticle() {
        articleViewModel.fetchAllArticle(completionHandler: { (allArticles) in
            self.articleData = allArticles
            self.tableView.reloadData()
        })
    }
    
    func getArticleByPagination(fetchMore: Int? = 1) {
        articleViewModel.fetchArticleData(page: fetchMore!, completionHandler: { (articles, arrPage) in
            self.modelPage = arrPage
            if self.isFetchMore == true {
                self.articleData.append(contentsOf: articles)
            } else {
                self.articleData = articles
            }
            self.tableView.reloadData()
        })
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell
        
        cell.contentView.backgroundColor = UIColor.white
        cell.articleImageView.kf.indicatorType = .activity
        cell.articleImageView.kf.setImage(with: URL(string: articleData[indexPath.row].imageUrl), placeholder: UIImage(named: "default_image"), options: nil, progressBlock: nil) { result in
            switch result {
            case .success( _):
                break
            case .failure(let error):
                if error.isInvalidResponseStatusCode {
                    cell.articleImageView.image = UIImage(named: "default_image")
                }
            }
        }
        
        cell.lblArticleTitle.text = articleData[indexPath.row].title
        cell.lblAuthorUsername.text = articleData[indexPath.row].authorName
        cell.authorImageView.image = UIImage(named: "\(articleData[indexPath.row].authorImage)")
        cell.lblArticleCreatedDate.text = articleData[indexPath.row].createdDate
        
        if articleData[indexPath.row].category?.name != "" {
            cell.lblCategoryName.text = articleData[indexPath.row].category?.name
        } else {
            cell.lblCategoryName.text = "General"
        }
        
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articleVC = storyboard?.instantiateViewController(withIdentifier: "articleVC") as! ArticleViewController
        articleDelegate = articleVC
        articleDelegate?.sendArticleData(id: articleData[indexPath.row].id, image: articleData[indexPath.row].imageUrl, title: articleData[indexPath.row].title, createdDate: articleData[indexPath.row].createdDate!, description: articleData[indexPath.row].description, authorName: self.articleData[indexPath.row].authorName, authorImage: self.articleData[indexPath.row].authorImage)
        articleVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(articleVC, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        let edit = UIAction(title: "edit".localizedString(), image: UIImage(systemName: "pencil.circle.fill")) { _ in
            let addArticleVC = self.storyboard?.instantiateViewController(withIdentifier: "addArticleVC") as! AddArticleViewController
            self.articleDelegate = addArticleVC
            self.articleDelegate?.sendArticleData(id: self.articleData[indexPath.row].id, image: self.articleData[indexPath.row].imageUrl, title: self.articleData[indexPath.row].title, createdDate: self.articleData[indexPath.row].createdDate!, description: self.articleData[indexPath.row].description, authorName: self.articleData[indexPath.row].authorName, authorImage: self.articleData[indexPath.row].authorImage)
            addArticleVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(addArticleVC, animated: true)
        }
        
        let delete = UIAction(title: "delete".localizedString(), image: UIImage(systemName: "trash.circle.fill"), attributes: .destructive) { _ in
            let loading = UIActivityIndicatorView(frame: CGRect(x: self.view.bounds.width * 0.5, y: self.view.bounds.height * 0.5, width: 0, height: 0))
            let transfrom = CGAffineTransform.init(scaleX: 2.2, y: 2.2)
            loading.transform = transfrom
            self.view.addSubview(loading)
            loading.startAnimating()
                        
            let alert = UIAlertController(title: "question_remove".localizedString(), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "yes".localizedString(), style: .destructive, handler: { _ in
                self.articleViewModel.deleteArticle(articleId: self.articleData[indexPath.row].id, completionHandler: { (deleteArticleResponse) in
                    loading.stopAnimating()
                    let deleteAlert = UIAlertController(title: "Article", message: deleteArticleResponse.message, preferredStyle: .alert)
                    deleteAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        self.articleData.remove(at: indexPath.row)
                    }))
                    self.present(deleteAlert, animated: true, completion: nil)
                })
            }))
            alert.addAction(UIAlertAction(title: "no".localizedString(), style: .default, handler: { _ in
                loading.stopAnimating()
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        let shareAction = UIAction(title: "share".localizedString(), image: UIImage(systemName: "arrowshape.turn.up.right.circle.fill")) { _ in
            print("Share")
        }

        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            return UIMenu(title: "article".localizedString(), children: [edit, delete, shareAction])
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItemOfArticle = articleData.count - 1
        if lastItemOfArticle == indexPath.row {
            isFetchMore = true
            page = page + 1
            getArticleByPagination(fetchMore: page)
        } else {
            if self.modelPage[0] > self.modelPage[1] {
                return
            }
        }
    }
    
}

