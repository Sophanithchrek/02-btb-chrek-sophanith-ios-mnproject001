//
//  Article.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import SwiftyJSON

struct Article {
    
    var id: String
    var title: String
    var description: String
    var published: Bool
    var imageUrl: String
    var categories: Categories?
    var author: Author?
    var createdDate: String
    var updatedDate: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
        self.published = json["published"].boolValue
        self.imageUrl = json["image"].stringValue
        self.categories = Categories(json: json["category"])
        self.author = Author(json: json["author"])
        self.createdDate = json["createdAt"].stringValue
        self.updatedDate = json["updatedAt"].stringValue
    }
    
}
