//
//  ArticleModel.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation

struct ArticleModel {
    
    var id: String
    var title: String
    var imageUrl: String
    var description: String
    var createdDate: String?
    var category: Categories?
    var authorName: String
    var authorImage: String

    init(article: Article) {
        self.id = article.id
        self.title = article.title
        self.imageUrl = article.imageUrl
        self.description = article.description
        
        let arrAuthorNameForRandom = ["Di Maria", "Mbappe", "CR7", "Messi", "Neymar", "Cavani", "Bruno", "Rooney"]
        let arrImageForRandom = ["avatar1", "avatar2", "avatar3", "avatar4", "avatar5", "avatar6", "avatar7", "avatar8", "avatar9", "avatar10"]
        
        self.authorName = arrAuthorNameForRandom.randomElement()!
        self.authorImage = arrImageForRandom.randomElement()!
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

        if let date = dateFormat.date(from: article.createdDate) {
            let newDate = DateFormatter()
            newDate.dateFormat = "dd-MMM-yyyy"
            self.createdDate = "Date: \(newDate.string(from: date))"
        }
        
        self.category = article.categories
    }
}
