//
//  ArticleRequest.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation

struct ArticleRequest: Codable {
    var title: String
    var description: String
    var published: Bool
    var imageUrl: String
}
