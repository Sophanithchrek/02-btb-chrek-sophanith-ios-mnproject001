//
//  ArticleDataResponse.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import SwiftyJSON

struct ArticleDataRespose {
    
    var data: [Article]
    var page: Int?
    var limit: Int?
    var totalPage: Int?
    var message: String
    
    init(json: JSON) {
        self.data = json["data"].arrayValue.compactMap(Article.init)
        self.page = json["page"].intValue
        self.limit = json["limit"].intValue
        self.totalPage = json["total_page"].intValue
        self.message = json["message"].stringValue
    }
}


