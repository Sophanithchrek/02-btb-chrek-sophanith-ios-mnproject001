//
//  Author.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import SwiftyJSON

struct Author {
    var id: String
    var name: String
    var email: String
    var imageUrl: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.name = json["name"].stringValue
        self.email = json["email"].stringValue
        self.imageUrl = json["image"].stringValue
    }
}
