//
//  Category.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import SwiftyJSON

struct Categories {
    
    var id: String
    var name: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.name = json["name"].stringValue
    }
}
