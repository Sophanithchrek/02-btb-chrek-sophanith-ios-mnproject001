//
//  ImageResponse.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import SwiftyJSON

struct ImageResponse {
    
    var id: String
    var imageUrl: String
    var createdAt: String
    var updatedAt: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.imageUrl = json["url"].stringValue
        self.createdAt = json["createdAt"].stringValue
        self.updatedAt = json["updatedAt"].stringValue
    }
    
}
