//
//  MessageResponse.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import SwiftyJSON

struct MessageResponse {
    var data: DataResponse
    var message: String
    
    init(json: JSON) {
        self.data = DataResponse(json: json["data"])
        self.message = json["message"].stringValue
    }
}

struct DataResponse {
    var id: String
    var title: String
    var description: String
    var published: Bool
    var imageUrl: String
    var createdAt: String
    var updatedAt: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
        self.published = json["published"].boolValue
        self.imageUrl = json["image"].stringValue
        self.createdAt = json["createdAt"].stringValue
        self.updatedAt = json["updatedAt"].stringValue
    }
}
