//
//  ArticleDelegate.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation

protocol ArticleDelegate {
    func sendArticleData(id: String, image: String, title: String, createdDate: String, description: String, authorName: String, authorImage: String)
}
