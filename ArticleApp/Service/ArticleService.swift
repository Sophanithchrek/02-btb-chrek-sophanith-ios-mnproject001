//
//  ArticleService.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import Alamofire
import SwiftyJSON

struct ArticleService {
    
    static let shared = ArticleService()
    let ARTICLE_MAIN_URL = "http://110.74.194.124:3000/api/articles"
    
    func fetchArticleData(page: Int, completionHandler: @escaping (_ articleDataRespose: ArticleDataRespose) -> Void) {
        AF.request("\(ARTICLE_MAIN_URL)?page=\(page)&size=10", method: .get).responseData { (baseDataResponse) in
            switch baseDataResponse.result {
            case .success(let value):
                let json = JSON(value)
                completionHandler(ArticleDataRespose(json: json))
            case .failure(_):
                print("Failed")
            }
        }
    }
    
    func fetchAllArticle(completionHandler: @escaping (_ response: [Article]) -> Void ) {
        AF.request("\(ARTICLE_MAIN_URL)?page=1&size=10", method: .get).responseData { (dataResponse) in
            switch dataResponse.result {
            case .success(let value):
                let json = JSON(value)
                var articles = [Article]()
                for (_, subJson):(String, JSON) in json["data"] {
                    articles.append(Article(json: subJson))
                }
                DispatchQueue.main.async {
                    completionHandler(articles)
                }
            case .failure(_):
                print("Failed")
            }
        }
    }
    
    func uploadImage(image: UIImage, completionHandler: @escaping(_ imageResonse: ImageResponse) -> Void) {
        let imgData = UIImage.jpegData(image) (compressionQuality: 0.5)
        AF.upload(multipartFormData:{ (formData) in
            formData.append(imgData!, withName: "image", fileName: ".jpg" , mimeType: "image/jpeg")
        }, to: "http://110.74.194.124:3000/api/images", method:.post).responseData { (result) in
            switch result.result {
            case .success(let value):
                let json = JSON(value)
                completionHandler(ImageResponse(json: json))
            case .failure(let err):
                print("Error Failure in Service", err)  }
        }
    }
    
    func postArticle(articleRequest: ArticleRequest, completionHandler: @escaping (_ messageResponse: MessageResponse) -> Void) {
        let articleParam: Parameters = [
            "title": articleRequest.title,
            "description": articleRequest.description,
            "published": true,
            "image": articleRequest.imageUrl
        ]
        
        AF.request("\(ARTICLE_MAIN_URL)", method: .post, parameters: articleParam, encoding: JSONEncoding.default).responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let value):
                completionHandler(MessageResponse(json: JSON(value)))
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func updateArticle(articleId: String, articleRequest: ArticleRequest, completionHandler: @escaping (_ messageResponse: MessageResponse) -> Void) {
        let articleParam: Parameters = [
            "title": articleRequest.title,
            "description": articleRequest.description,
            "published": true,
            "image": articleRequest.imageUrl
        ]
        
        AF.request("\(ARTICLE_MAIN_URL)/\(articleId)", method: .patch, parameters: articleParam, encoding: JSONEncoding.default).responseJSON (completionHandler: { response in
            switch response.result {
            case .success(let value):
                completionHandler(MessageResponse(json: JSON(value)))
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func deleteArticle(articleId: String, completionHandler: @escaping (_ messageResponse: MessageResponse) -> Void) {
        AF.request("\(ARTICLE_MAIN_URL)/\(articleId)", method: .delete).response { response in
            switch response.result {
            case .success(let value):
                completionHandler(MessageResponse(json: JSON(value!)))
            case .failure(let error):
                print(error)
            }
        }
    }

}
