//
//  ArticleViewModel.swift
//  ArticleApp
//
//  Created by SOPHANITH CHREK on 19/12/20.
//

import Foundation
import UIKit

struct ArticleViewModel {
    
    let articleService = ArticleService.shared
    static let shared = ArticleViewModel()
    
    func fetchArticleData(page: Int, completionHandler: @escaping (_ articleModel: [ArticleModel], [Int]) -> Void) {
        articleService.fetchArticleData(page: page, completionHandler: { (articleDataResponse) in
            var articleModels = [ArticleModel]()
            for rec in articleDataResponse.data {
                articleModels.append(ArticleModel(article: rec))
            }
            completionHandler(articleModels, [articleDataResponse.page!, articleDataResponse.totalPage!])
        })
    }
    
    func fetchAllArticle(completionHandler: @escaping (_ article: [ArticleModel]) -> Void) {
        articleService.fetchAllArticle(completionHandler: { (articleResponse) in
            var articleModels = [ArticleModel]()
            DispatchQueue.main.async {
                articleModels = articleResponse.compactMap(ArticleModel.init)
                completionHandler(articleModels)
            }
        })
    }
    
    func uploadImage(image: UIImage, completionHandler: @escaping (_ imageResponse: ImageResponse) -> Void) {
        articleService.uploadImage(image: image, completionHandler: { (imageResponse) in
            DispatchQueue.main.async {
                completionHandler(imageResponse)
            }
        })
    }
    
    func postArticle(article: ArticleRequest, completionHandler: @escaping (_ messageResponse: MessageResponse) -> Void) {
        articleService.postArticle(articleRequest: article, completionHandler: { (postArticleResponse) in
            DispatchQueue.main.async {
                completionHandler(postArticleResponse)
            }
        })
    }
    
    func updateArticle(articleId: String, article: ArticleRequest, completionHandler: @escaping (_ messageResponse: MessageResponse) -> Void) {
        articleService.updateArticle(articleId: articleId, articleRequest: article, completionHandler: { (updateArticleResponse) in
            DispatchQueue.main.async {
                completionHandler(updateArticleResponse)
            }
        })
    }
    
    func deleteArticle(articleId: String, completionHandler: @escaping (_ messageResponse: MessageResponse) -> Void) {
        articleService.deleteArticle(articleId: articleId, completionHandler: { (deleteArticleResponse) in
            DispatchQueue.main.async {
                completionHandler(deleteArticleResponse)
            }
        })
    }
    
}
